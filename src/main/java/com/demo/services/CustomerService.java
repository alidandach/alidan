package com.demo.services;

import com.demo.entities.Customer;
import com.demo.entities.Invoice;
import com.demo.entities.dto.BusinessObject;
import com.demo.entities.dto.CustomerDto;
import com.demo.entities.dto.InvoiceDto;
import com.demo.entities.dto.StatusCode;
import com.demo.exceptions.GlobalException;
import com.demo.repositories.CustomerMapper;
import com.demo.repositories.InvoiceMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.function.Function;
import java.util.stream.Collectors;

@Service public class CustomerService
{

	@Autowired private CustomerMapper customerMapper;

	@Autowired private InvoiceMapper invoiceMapper;
	private Function<Customer, CustomerDto> customerDataToCustomerDto = customer -> new CustomerDto()
			.setId(customer.getId())
			.setFirstName(customer.getFirstName())
			.setLastName(customer.getLastName())
			.setEmail(customer.getEmail())
			.setInvoices(customer
					.getInvoices()
					.parallelStream()
					.map(i -> new InvoiceDto()
							.setId(i.getInvoiceId())
							.setAmount(i.getAmount())
							.setQuantity(i.getQuantity()))
					.collect(Collectors.toList()));
	private Function<CustomerDto, Customer> customerDtoToCustomerData = dto -> new Customer()
			.setId(dto.getId())
			.setFirstName(dto.getFirstName())
			.setLastName(dto.getLastName())
			.setEmail(dto.getEmail())
			.setInvoices(dto.getInvoices() != null
						 ? dto
								 .getInvoices()
								 .parallelStream()
								 .map(i -> new Invoice()
										 .setInvoiceId(i.getId())
										 .setAmount(i.getAmount())
										 .setQuantity(i.getQuantity()))
								 .collect(Collectors.toList())
						 : null);
	private Function<InvoiceDto, Invoice> invoiceDtoToInvoiceData = dto -> new Invoice()
			.setInvoiceId(dto.getId())
			.setQuantity(dto.getQuantity())
			.setAmount(dto.getAmount());

	/**
	 * save customer
	 *
	 * @param input CustomerDto
	 */
	@Transactional(rollbackFor = Exception.class)
	public BusinessObject saveCustomer(CustomerDto input)
	{
		try
		{
			customerMapper.save(customerDtoToCustomerData.apply(input));
			//businessObject.data = customerDataToCustomerDto.apply(customer);
			return new BusinessObject()
					.setStatusCode(StatusCode.SUCCESS.getCode())
					.setMessage(StatusCode.SUCCESS.getMessage());

		}
		catch(Exception e)
		{
			throw new GlobalException(e);
		}
	}

	/**
	 * get customer using id
	 *
	 * @param id int
	 * @return CustomerDto
	 */
	@Transactional(rollbackFor = Exception.class)
	public BusinessObject getCustomer(int id)
	{
		try
		{
			Customer data = customerMapper.findById(id);
			return data != null
				   ? new BusinessObject()
						   .setData(customerDataToCustomerDto.apply(data))
						   .setStatusCode(StatusCode.SUCCESS.getCode())
						   .setMessage(StatusCode.SUCCESS.getMessage())
				   : new BusinessObject()
						   .setStatusCode(StatusCode.NOT_FOUND.getCode())
						   .setMessage(StatusCode.NOT_FOUND.getMessage());

		}
		catch(Exception e)
		{
			throw new GlobalException(e);
		}
	}

	/**
	 * edit customer
	 *
	 * @param in CustomerDto
	 */
	@Transactional(rollbackFor = Exception.class)
	public BusinessObject edit(CustomerDto in)
	{
		try
		{
			customerMapper.update(customerDtoToCustomerData.apply(in));

			return new BusinessObject()
					.setData(in)
					.setStatusCode(StatusCode.SUCCESS.getCode())
					.setMessage(StatusCode.SUCCESS.getMessage());

		}
		catch(Exception e)
		{
			throw new GlobalException(e);
		}
	}

	/**
	 * get all customers
	 *
	 * @return list of Customer Dto
	 */
	@Transactional(rollbackFor = Exception.class)
	public BusinessObject getCustomers()
	{
		try
		{
			return new BusinessObject()
					.setDataList(customerMapper
							.findAll()
							.stream()
							.map(i -> customerDataToCustomerDto.apply(i))
							.collect(Collectors.toList()))
					.setStatusCode(StatusCode.SUCCESS.getCode())
					.setMessage(StatusCode.SUCCESS.getMessage());
		}
		catch(Exception e)
		{
			throw new GlobalException(e);
		}
	}

	/**
	 * delete customer using id
	 *
	 * @param id int
	 */
	@Transactional(rollbackFor = Exception.class)
	public BusinessObject delete(int id)
	{
		try
		{
			return new BusinessObject()
					.setData(customerDataToCustomerDto.apply(customerMapper.findById(id)))
					.setStatusCode(StatusCode.SUCCESS.getCode())
					.setMessage(StatusCode.SUCCESS.getMessage());
		}
		catch(Exception e)
		{
			throw new GlobalException(e);
		}
	}

	/**
	 * add invoice for specific customer
	 *
	 * @param customerId int
	 * @param invoiceDto dto
	 */
	@Transactional(rollbackFor = Exception.class)
	public BusinessObject addInvoice(int customerId, InvoiceDto invoiceDto)
	{
		try
		{

			invoiceMapper.save(new Invoice()
					.setInvoiceId(0)
					.setAmount(invoiceDto.getAmount())
					.setQuantity(invoiceDto.getQuantity())
					.setCustomer(new Customer().setId(customerId)));

			return new BusinessObject()
					.setData(customerDataToCustomerDto.apply(customerMapper.findById(customerId)))
					.setStatusCode(StatusCode.SUCCESS.getCode())
					.setMessage(StatusCode.SUCCESS.getMessage());

		}
		catch(Exception e)
		{
			throw new GlobalException(e);
		}
	}

	/**
	 * update specific invoice
	 *
	 * @param invoice dto
	 */
	@Transactional(rollbackFor = Exception.class)
	public BusinessObject updateInvoice(int customerId, InvoiceDto invoice)
	{
		try
		{
			invoiceMapper.update(invoiceDtoToInvoiceData.apply(invoice));

			return new BusinessObject()
					.setData(customerDataToCustomerDto.apply(customerMapper.findById(customerId)))
					.setStatusCode(StatusCode.SUCCESS.getCode())
					.setMessage(StatusCode.SUCCESS.getMessage());

		}
		catch(Exception e)
		{
			throw new GlobalException(e);
		}
	}

	//    Function<Invoice, InvoiceDto> invoiceDataToInvoiceDto = invoice -> new InvoiceDto().setId(invoice.getInvoiceId())
	//            .setQuantity(invoice.getQuantity()).setAmount(invoice.getAmount());

	/**
	 * delete specific invoice
	 *
	 * @param invoiceId int
	 */
	@Transactional(rollbackFor = Exception.class)
	public BusinessObject deleteInvoice(int customerId, int invoiceId)
	{
		try
		{
			invoiceMapper.deleteById(invoiceId);

			return new BusinessObject()
					.setData(customerDataToCustomerDto.apply(customerMapper.findById(customerId)))
					.setStatusCode(StatusCode.SUCCESS.getCode())
					.setMessage(StatusCode.SUCCESS.getMessage());

		}
		catch(Exception e)
		{
			throw new GlobalException(e);
		}
	}
}
