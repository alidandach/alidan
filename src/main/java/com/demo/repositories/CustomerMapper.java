package com.demo.repositories;

import com.demo.entities.Customer;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository @Mapper public interface CustomerMapper
{

	int save(Customer in);

	void update(Customer in);

	Customer findById(int id);

	List<Customer> findAll();

	void deleteById(int id);

}
