package com.demo.repositories;

import com.demo.entities.Invoice;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

@Repository @Mapper public interface InvoiceMapper
{
	void save(Invoice invoice);

	void update(Invoice invoice);

	void deleteById(int id);
}
