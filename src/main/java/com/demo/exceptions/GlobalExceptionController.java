package com.demo.exceptions;

import com.demo.entities.dto.BusinessObject;
import com.demo.entities.dto.StatusCode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice public class GlobalExceptionController
{
	private Logger logger = LoggerFactory.getLogger(GlobalExceptionController.class);

	@ExceptionHandler(value = GlobalException.class)
	public ResponseEntity<Object> exception(GlobalException exception)
	{
		logger.error(exception.getMessage());
		logger.info(exception.getMessage());

		return new ResponseEntity<>(new BusinessObject().setStatusCode(StatusCode.UNSUCCESS.getCode()).setMessage(StatusCode.UNSUCCESS.getMessage()),
				HttpStatus.NOT_FOUND);
	}
}
