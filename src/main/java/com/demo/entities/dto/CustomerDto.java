package com.demo.entities.dto;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

import java.util.List;

@Getter @Setter @Accessors(chain = true) public class CustomerDto implements Data
{
	private Integer id;
	private String firstName;
	private String lastName;
	private String email;
	private List<InvoiceDto> invoices;
}
