package com.demo.entities.dto;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

@Getter @Setter @Accessors(chain = true) public class InvoiceDto implements Data
{
	private int id;
	private int quantity;
	private double amount;
}
