package com.demo.entities.dto;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

import java.util.List;

@Getter @Setter @Accessors(chain = true) public class BusinessObject
{
	private Data data;
	private List<Data> dataList;
	private int statusCode;
	private String message;
}
