package com.demo.entities.dto;

public enum StatusCode
{
	SUCCESS(1, "success"), UNSUCCESS(-1, "unsuccess"), NOT_FOUND(2, "record not found");

	private int code;
	private String message;

	StatusCode(int code, String message)
	{
		this.code = code;
		this.message = message;
	}

	public int getCode()
	{
		return code;
	}

	public String getMessage()
	{
		return message;
	}
}
