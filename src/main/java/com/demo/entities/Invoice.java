package com.demo.entities;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

@Getter @Setter @Accessors(chain = true) public class Invoice
{
	private int invoiceId;
	private int quantity;
	private double amount;
	private Customer customer;
}
