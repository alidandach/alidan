package com.demo.entities;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

import java.util.List;

@Getter @Setter @Accessors(chain = true) public class Customer
{
	private int id;
	private String firstName;
	private String lastName;
	private String email;
	private List<Invoice> invoices;

	public void addInvoice(Invoice invoice)
	{
		invoices.add(invoice);
	}

	public void removeInvoice(Invoice invoice)
	{
		invoices.remove(invoice);
	}
}
