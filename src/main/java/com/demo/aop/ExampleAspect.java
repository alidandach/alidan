package com.demo.aop;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.reflect.MethodSignature;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.util.Arrays;

@Aspect @Component public class ExampleAspect
{
	Logger logger = LoggerFactory.getLogger(ExampleAspect.class);

	@Before(value = "execution(* com.demo.controllers.CustomerController.*(..))")
	public void logEntry(JoinPoint joinPoint)
	{
		logger.info("Entering method with parameters" + Arrays.toString(((MethodSignature) joinPoint.getSignature()).getParameterNames()));
	}

	@After(value = "execution(* com.demo.controllers.CustomerController.*(..))")
	public void logExit(JoinPoint joinPoint)
	{
		logger.info("Exiting method " + joinPoint.getSignature().getName());
	}

	@AfterReturning(value = "execution(* com.demo.controllers.CustomerController.*(..))")
	public void logExitAfterReturn(final JoinPoint joinPoint)
	{
		logger.info("Exiting method (after return) " + ((MethodSignature) joinPoint.getSignature()).getReturnType());
	}

}
