package com.demo.controllers;

import com.demo.entities.dto.BusinessObject;
import com.demo.entities.dto.CustomerDto;
import com.demo.entities.dto.InvoiceDto;
import com.demo.services.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController @RequestMapping("customers") public class CustomerController
{

	@Autowired private CustomerService customerService;

	/**
	 * add new customer
	 *
	 * @param customer customer dto
	 */
	@RequestMapping(value = "", method = RequestMethod.POST)
	@ResponseBody
	public BusinessObject add(@RequestBody CustomerDto customer)
	{
		return customerService.saveCustomer(customer);
	}

	/**
	 * find by id
	 *
	 * @param id id of customer
	 * @return customer dto
	 */
	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	@ResponseBody
	public BusinessObject getCustomer(@PathVariable("id") int id)
	{
		return customerService.getCustomer(id);
	}

	/**
	 * update customer without invoices
	 *
	 * @param customer dto of customer
	 */
	@RequestMapping(value = "/{id}", method = RequestMethod.PUT)
	@ResponseBody
	public BusinessObject edit(@RequestBody CustomerDto customer)
	{
		return customerService.edit(customer);
	}

	/**
	 * delete customer with invoices
	 *
	 * @param id id of customer
	 */
	@DeleteMapping(value = "/{id}")
	public BusinessObject delete(@PathVariable int id)
	{
		return customerService.delete(id);
	}

	/**
	 * find all
	 *
	 * @return List of customers with invoices
	 */
	@GetMapping(value = "")
	public BusinessObject getCustomers()
	{
		return customerService.getCustomers();
	}

	/**
	 * add invoice to specific customer
	 */
	@PostMapping(value = "/{id}/invoices")
	public BusinessObject addInvoice(@PathVariable("id") int customerId, @RequestBody InvoiceDto invoice)
	{
		return customerService.addInvoice(customerId, invoice);
	}

	/**
	 * update specific invoice for specific customer
	 */
	@PutMapping(value = "/{customerId}/invoices/{invoiceId}")
	public BusinessObject updateInvoice(@PathVariable("customerId") int customerId, @PathVariable("invoiceId") int invoiceId,
			@RequestBody InvoiceDto invoice)
	{
		return customerService.updateInvoice(customerId, invoice.setId(invoiceId));
	}

	/**
	 * delete specific invoice for specific customer
	 */
	@DeleteMapping(value = "/{customerId}/invoices/{invoiceId}")
	public BusinessObject deleteInvoice(@PathVariable("customerId") int customerId, @PathVariable("invoiceId") int invoiceId)
	{
		return customerService.deleteInvoice(customerId, invoiceId);
	}

}
