import com.Application;
import com.demo.entities.dto.BusinessObject;
import com.demo.entities.dto.CustomerDto;
import com.demo.services.CustomerService;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

@RunWith(SpringRunner.class) @SpringBootTest(classes = Application.class) public class TestExample
{

	@Autowired CustomerService customerService;

	/*
	 * strategy used:
	 * 1- create new customer
	 * 2- save the new customer
	 * 3- get all customers in database
	 * 4- assert if total of customer equal to one ( database empty )
	 */
	@Test
	@Transactional
	public void save()
	{
		customerService.saveCustomer(new CustomerDto()
				.setId(0)
				.setFirstName("test firstName")
				.setLastName("testLastName")
				.setEmail("test@junit.com"));

		BusinessObject businessObject = customerService.getCustomers();

		Assert.assertEquals(businessObject
				.getDataList()
				.size(), 1);

	}
}
